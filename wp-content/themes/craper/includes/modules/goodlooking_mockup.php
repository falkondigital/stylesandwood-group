<?php $t = $GLOBALS['_sh_base'];
ob_start();
?>

<div class="col-xs-12">
	<h4><?php echo $title;?></h4>
	<p><?php echo $text;?></p>
	<div class="display">
		<figure> <img src="<?php echo wp_get_attachment_url( $img1, 'full' ); ?>" alt="image"> <img src="<?php echo wp_get_attachment_url( $img2, 'full' ); ?>" alt="image" class="bottom-right"> </figure>
	</div>
</div>

<?php  
$output = ob_get_contents();
ob_end_clean();

?>