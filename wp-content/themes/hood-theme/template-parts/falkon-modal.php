<style type="text/css">
    /* overwrite */
    .scrollActive .header_hood_sticky {
        z-index:100 !important;
    }

    .modal {
        text-align: center;
        padding: 0!important;
        z-index:9999 !important;
    }
    .modal .close {
        font-size:24px;
    }
    .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
    }

    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }
    .modal-body {
        padding:25px 15px;
    }
    #para-entry p {
        line-height:24px;
        font-size:18px;
        padding:15px;
    }
    a.flkn-blue-btn {
        background-color: rgba(21, 102, 170, 1);
        color:#fff;
        font-size:16px;
        font-family: Raleway;
        padding:15px 20px;
        border:2px solid rgba(21, 102, 170, 1);
        border-radius:2px;
    }
    a.flkn-blue-btn:hover {
        background-color: #fff;
        color:rgba(21, 102, 170, 1) !important;
        font-family: Raleway;
        border:2px solid rgba(21, 102, 170, 1);
    }
    .rightsep {
        border-right:1px solid #c2c2c2;
    }
</style>
<?php if( is_front_page() ) { ?>
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">

                    <div class="row">
                    <div class="col-xs-12 col-md-12 text-right" data-dismiss="modal" aria-hidden="true">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                    </div>
                    </div>

                    <div class="row">
                    <div id="para-entry" class="col-xs-12 col-sm-12 text-center">
                        <p>For information relating to Central Square Holdings Limited’s proposed transaction with Styles&Wood, please select the transaction page using the button below.
                        Alternatively, proceed to the Styles&Wood Group website.</p>
                    </div>
                    </div>

                    <div class="row">
                    <div class="col-xs-12 col-md-6 text-center rightsep">
                        <a class="btn btn-primary flkn-blue-btn"  data-dismiss="modal" aria-hidden="true">
                            Continue to the website
                        </a>
                    </div>

                    <div class="col-xs-12 col-md-6 text-center">
                        <a href="<?php echo esc_attr(site_url('/transaction-disclaimer')); ?>" class="btn btn-primary flkn-blue-btn">
                            Go to transaction page
                        </a>
                    </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php } ?>